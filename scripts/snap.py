import resinit
from dateutil import parser
from datetime import datetime, timedelta

# local variables
max_snapshot_days_limit = 30

# empty variables
snapshot_list = []
removed_snapshot_list = []

# create snapshot
def create_snapshot(ec2_conn, ec2_instances, backup_frequency, snap_retention=0, snap_cleanup=False, cleanup_filter='DoNotDelete'):
    for instance in ec2_instances:
        print('**************** processing instance:', instance['InstanceId'], '****************')
        for tag in instance['Tags']:
            if tag['Key'] == 'Name':
                instance_name = tag['Value']
            if tag['Key'] == 'snap_' + backup_frequency:
                snap_retention = tag['Value']
        print(backup_frequency, ' retention for instance_id ', instance['InstanceId'], ' is: ', snap_retention)

        # validate retention period
        if 0 < int(snap_retention) < max_snapshot_days_limit:
            print('Info: Retention period for instance id', instance['InstanceId'], 'is positive integer => '
                  , snap_retention, ', Go ahead.')
            snapshot_name_tag = cleanup_filter + ' of ' + instance_name + ' having id: ' + instance['InstanceId']
            resinit.ebs_snapshot(ec2_conn, instance, snapshot_name_tag, instance_name)

            # call deletion function
            if snap_cleanup:
                delete_snapshot(ec2_conn, snap_retention, snapshot_name_tag)
        else:
            print('Warning: Retention period for instance_id ', instance['InstanceId'], 'is invalid => '
                  , snap_retention, ', Skipping.')
    return snapshot_reporting(snapshot_list, removed_snapshot_list)


# delete snapshot
def delete_snapshot(ec2_conn, snap_retention, desc_filter):
    print('starting snapshot cleanup...')
    retention_period = parser.parse(str(datetime.utcnow() - timedelta(days=int(snap_retention)))).date()
    for snapshot in resinit.list_ebs_snapshots(ec2_conn, 'description', desc_filter):
        snapshot_list.append(snapshot['SnapshotId'])
        snapshot_create_date = parser.parse(str(snapshot['StartTime'])).date()
        if snapshot_create_date <= retention_period:
            removed_snapshot_list.append(snapshot['SnapshotId'])
            ec2_conn.delete_snapshot(SnapshotId=snapshot['SnapshotId'])
            print(snapshot['SnapshotId'], 'creation date is', snapshot_create_date, 'against a retention period of',
                  snap_retention, ' day(s) ::  Well! just get removed.')
        else:
            print(snapshot['SnapshotId'], 'creation date is within limit, so skipping deletion.')
    return removed_snapshot_list, snapshot_list


def snapshot_reporting(snapshot_count, removed_snapshot_count):
    print('Total number of auto snapshots: ', len(snapshot_count))
    print('Number of deleted snapshots: ', len(removed_snapshot_count))
