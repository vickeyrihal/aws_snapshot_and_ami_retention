# http://www.bogotobogo.com/python/python_argparse.php
import argparse
import sys

def check_arg(args=None):
    parser = argparse.ArgumentParser(description='script triggering ami backup')
    parser.add_argument('-r', '--region',
                        help='aws region code',
                        default='eu-west-1')
    parser.add_argument('-t', '--type',
                        help='backup type - snapshot, ami',
                        required='True')
    parser.add_argument('-f', '--frequency',
                        help='script frequence - daily, weekly, yearly',
                        required='True')
    parser.add_argument('-k', '--filter',
                        help='Tag Key for filtering purpose',
                        required='True')
    results = parser.parse_args(args)
    return results.region, results.type, results.frequency, results.filter
