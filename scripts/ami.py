import resinit
from dateutil import parser
from datetime import datetime, timedelta

# local variables
max_ami_days_limit = 30
tag_filter = 'snap_*'

# empty variables
ami_list = []
removed_ami_list = []
removed_ami_snap_list = []


# create snapshot - daily
def create_ami(ec2_conn, ec2_instances, backup_frequency, ami_retention=0, ami_cleanup=False, cleanup_filter='DoNotDelete'):
    for instance in ec2_instances:
        print('**************** processing instance for snapshot:', instance['InstanceId'], '****************')
        for tag in instance['Tags']:
            if tag['Key'] == 'Name':
                instance_name = tag['Value']
            if tag['Key'] == 'ami_' + backup_frequency:
                ami_retention = tag['Value']
        print(backup_frequency, ' retention for instance_id ', instance['InstanceId'], ' is: ', ami_retention)

        # validate retention period
        if 0 < int(ami_retention) < max_ami_days_limit:
            print('Info: Retention period for instance id', instance['InstanceId'], 'is positive integer => '
                  , ami_retention, ', Go ahead.')
            ami_name_tag = cleanup_filter + ' of ' + instance_name + ' having id: ' + instance['InstanceId']
            resinit.create_ami(
                ec2_conn, instance, ami_name_tag, instance_name
            )

            # call deletion function
            if ami_cleanup:
                delete_ami(ec2_conn, backup_frequency, ami_retention, ami_name_tag)
        else:
            print('Warning: Retention period for instance_id ', instance['InstanceId'], 'is invalid => '
                  , ami_retention, ', Skipping.')
    return ami_reporting(ami_list, removed_ami_list, removed_ami_snap_list)


# delete snapshot
def delete_ami(ec2_conn, backup_frequency, ami_retention, desc_filter):
    print('starting ami cleanup...')
    if backup_frequency == 'daily':
        retention_period = parser.parse(str(datetime.utcnow() - timedelta(days=int(ami_retention)))).date()
    elif backup_frequency == 'weekly':
        # convert to ami retention in days
        ami_retention_weekly = int(ami_retention) * int(7)
        retention_period = parser.parse(str(datetime.utcnow() - timedelta(days=int(ami_retention_weekly)))).date()

    ami_all = resinit.list_ami(ec2_conn, 'description', desc_filter)

    for image in ami_all:
        ami_list.append(image['ImageId'])
        ami_create_date = parser.parse(str(image['CreationDate'])).date()
        if ami_create_date <= retention_period:
            # de-register AMIs
            removed_ami_list.append(image['ImageId'])
            print('de-registering ami id: ', image['ImageId'])
            ec2_conn.deregister_image(
                ImageId = image['ImageId']
            )
            # remove associated snapshots
            block_mapping = image['BlockDeviceMappings']
            for block_storage in block_mapping:
                if 'Ebs' in block_storage:
                    removed_ami_snap_list.append(block_storage['Ebs']['SnapshotId'])
                    ec2_conn.delete_snapshot(
                        SnapshotId=block_storage['Ebs']['SnapshotId']
                    )
    return ami_list, removed_ami_list, removed_ami_snap_list


def ami_reporting(ami_count, removed_ami_count, removed_ami_snap_count):
    print('Total number of auto images: ', len(ami_count))
    print('Number of deleted images: ', len(removed_ami_count))
    print('Number of deleted snapshots associated to images: ', len(removed_ami_snap_count))
