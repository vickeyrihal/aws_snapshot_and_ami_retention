# AWS Resource Initialization
import boto3
import logging
from datetime import datetime

# key =
# secret =
aws_account = 'XXX52271XX5'

logging.basicConfig(filename="snap.log", level=logging.INFO)


# boto3.set_stream_logger('boto3.client', logging.basicConfig(filename='D:\snap.log', level=logging.INFO))

def ec2_connection(region):
    ec2 = boto3.client('ec2',
                       region_name=region)
    """ :type : pyboto3.ec2 """
    return ec2


# EC2 connection
def get_ec2_instances(ec2, tag_filter):
    try:
        # reservations - ec2
        reservations = ec2.describe_instances(
            Filters=[
                {'Name': 'tag-key', 'Values': [tag_filter]},
            ]
        ).get(
            'Reservations', []
        )

        # List of instances
        instances = sum(
            [
                [i for i in r['Instances']]
                for r in reservations
                ], [])
    except IndexError:
        print('Unexpected error at conn.py')
    return instances


# EBS Snapshot specific
def ebs_snapshot(ec2, instance, instance_desc, instance_name):
    for dev in instance['BlockDeviceMappings']:
        # skip if there is no Ebs key
        if dev.get('Ebs', None) is None:
            continue
        vol_id = dev['Ebs']['VolumeId']
        print('Found EBS volume %s on instance %s' % (vol_id, instance['InstanceId']))
        # take snapshot
        snap = ec2.create_snapshot(
            # DryRun=True,
            VolumeId=vol_id,
            Description=instance_desc,
        )
        ec2.create_tags(Resources=[snap['SnapshotId']], Tags=[{'Key': 'Name', 'Value': instance_name + ' ' + vol_id}])
        print('snapshot against volume %s in instance %s is: %s' % (vol_id, instance['InstanceId'], snap['SnapshotId']))
    return vol_id


def list_ebs_snapshots(ec2, tag_key, tag_value):
    list_private_snapshots = ec2.describe_snapshots(
        Filters=[
            {
                'Name': tag_key,
                'Values': [tag_value]
            },
        ],
        OwnerIds=[
            aws_account,
        ],
    ).get(
        'Snapshots', []
    )
    return list_private_snapshots


# AMI specific
def create_ami(ec2, instance, instance_desc, instance_name):
    # cut ami
    ami = ec2.create_image(
        InstanceId=instance['InstanceId'],
        Name=instance_name + str(datetime.now().strftime("%Y%m%d%H%M")),
        Description=instance_desc,
        NoReboot=True,
    )
    ec2.create_tags(Resources=[ami['ImageId']], Tags=[{'Key': 'Name', 'Value': instance_name}])
    print('AMI created against instance %s is: %s' % (instance['InstanceId'], ami['ImageId']))
    return ami['ImageId']


def list_ami(ec2, tag_key, tag_value):
    list_private_ami = ec2.describe_images(
        Owners=[
            "self",
        ],
        Filters=[
            {
                'Name': tag_key,
                'Values': [tag_value],
            }
        ]
    ).get(
        'Images', []
    )
    return list_private_ami
