# arguments => -t snapshot -f daily -k snap_daily
# arguments => -t ami -f daily -k ami_daily
import sys

sys.path.append( 'scripts' )

import args
import ami
import resinit
import snap

# process the arguments
region_name, backup_type, backup_frequency, tag_filter = args.check_arg(sys.argv[1:])

# make ec2 connection
ec2 = resinit.ec2_connection(region_name)

# validate & process
if backup_type == 'ami' and backup_frequency == 'weekly':
    print('backup type is', backup_type, ' and backup_frequency is ', backup_frequency)
    ec2_instances = resinit.get_ec2_instances(ec2, tag_filter)
    ami.create_ami(ec2, ec2_instances, backup_frequency, ami_cleanup=True, cleanup_filter='Scheduled_Weekly_Backup')

elif backup_type == 'snapshot' and backup_frequency == 'daily':
    print('backup type is "', backup_type, '" and backup_frequency is "', backup_frequency)
    ec2_instances = resinit.get_ec2_instances(ec2, tag_filter)
    snap.create_snapshot(ec2, ec2_instances, backup_frequency, snap_cleanup=True, cleanup_filter='Scheduled_Daily_Backup')
else:
    print('script frequency is not valid.. exiting with status code 1')
    sys.exit(1)

